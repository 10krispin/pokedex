 package com.example.appcristian


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color.*
import androidx.compose.ui.layout.ContentScale

import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.appcristian.Modelo.getMedia
import com.example.appcristian.Modelo.itemPokemon

 class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent { 
                ListaElementos()
            }
        }
    }

 @Preview(showBackground = true)
 @Composable
 fun ListaElementos()
 {
     //diseño para cada columna
     LazyColumn(
         contentPadding = PaddingValues(4.dp),
         verticalArrangement = Arrangement.spacedBy(6.dp)
     )
     {
         //con getmedia se obtienen una instancia de los item a utilizar
         items(getMedia()){ items ->
             VistaListaPokemon(items)
         }
     }
 }


// @Preview(showBackground = true)
@Composable
fun VistaListaPokemon(items: itemPokemon)
{
    Column() {
        Box(
            modifier = Modifier
                .clickable { }
                .height(200.dp)
                .fillMaxWidth()
           )
            {
                //painterresource permite agregar una imagen y desde la intancia items se manda a traer la imagen
                Image(painter = painterResource(items.image),
                    contentDescription = null,
                    modifier = Modifier.fillMaxSize(),
                    contentScale = ContentScale.Crop)
            }

        Box(
            modifier= Modifier
                .fillMaxWidth()
                .background(color = Companion.Cyan)
                .padding(9.dp),
            contentAlignment = Alignment.Center
            )
                {
                    Text(text = items.name
                    ,style=MaterialTheme.typography.h6 )
                }

    } //columna
    
}

 //@Preview(widthDp = 400, heightDp = 200, showBackground = true)
 @Composable
 fun botonTexto()
 {
     Box(modifier = Modifier.fillMaxSize(),
         contentAlignment = Alignment.Center )
     {
         Text(text = "BOTON ACEPTAR",
             modifier= Modifier
                 .clickable { }
                 .background(color = Companion.Blue)
                 .border(width = 2.dp, color = Companion.Red)
                 .padding(horizontal = 16.dp, vertical = 8.dp))
     }

 }
